import { Component, inject } from '@angular/core';

import { ProductService } from '../product.service';
import { ControlType } from 'app/shared/utils/crud-item-options/control-type.model';


@Component({
  selector: 'app-products-admin',
  templateUrl: './products-admin.component.html',
  styleUrls: ['./products-admin.component.scss']
})
export class ProductsAdminComponent {

  readonly productService = inject(ProductService);

  product$ = this.productService.getProducts();

  crudItemOptions = [
    {
      key: 'code',
      label: 'code',
      type: 'text',
      controlType: ControlType.INPUT,
      columnOptions: {
        default: true
      }
    },
    {
      key: 'name',
      label: 'name',
      type: 'text',
      controlType: ControlType.INPUT,
      columnOptions: {
        default: true
      }
    }
  ];

  onSaved(data): void {
   this.product$ = this.productService.saveProduct(data);
  }

  onDeleted(ids: number[]): void {
    this.product$ = this.productService.deleteProducts(ids);
   }

}
