import { Injectable } from '@angular/core';
import { BehaviorSubject, map, tap } from 'rxjs';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Product } from './model/product.model';

@Injectable({
    providedIn: 'root'
})
export class ProductService {
    private URL = '../assets/products.json';
    private products = new BehaviorSubject<Product[]>([]);

    constructor(private http: HttpClient) {
    }

    getProducts(): Observable<Product[]> {
        return this.http.get(this.URL)
            .pipe(
                map((result: {data: Product[]}) => result.data),
                tap((products) => this.products.next(products))
            ); 
    }

    deleteProducts(ids: number[]): Observable<Product[]> {
       const products = this.products.value.filter((product:Product) => ids.includes(product.id) === false);
       this.products.next(products)

       return this.products.asObservable(); 
    }

    saveProduct(product: Product): Observable<Product[]> {
      const products = this.products.value;
      const pos = products.findIndex((item:Product) => item.code === product.code);

      if(pos < 0) {
        // add product
        products.push(product);
      } else {
        // update product
        products[pos] = {...products[pos], ...product };
      }

      this.products.next(products)

      return this.products.asObservable();
    }
}
