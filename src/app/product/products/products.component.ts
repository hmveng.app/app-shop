import { Component, inject } from '@angular/core';

import { ProductService } from '../product.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent {

  readonly productService = inject(ProductService);

  product$ = this.productService.getProducts();

  sortOptions = [
    { label: 'Bonne note', value: 'desc-rating' },
    { label: 'Mauvaise note', value: 'asc-rating' }
  ];

  onDeleteClicked(products) {
    const productIds = products.map((item) => item.id);
    this.product$ = this.productService.deleteProducts(productIds);
  }

}
