export class Product {
    id: number;
    code: string;
    name: string;
    description: string;
    image?: string;
    price: string;
    category: string;
    quantity: number;
    inventoryStatus: string;
    rating?: number;
  }